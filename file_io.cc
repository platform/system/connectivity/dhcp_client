//
// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "dhcp_client/file_io.h"

#include <string>

#include <base/files/file_util.h>

namespace dhcp_client {

namespace {
base::LazyInstance<FileIO> g_file_io
    = LAZY_INSTANCE_INITIALIZER;
}  // namespace

FileIO::FileIO() {}
FileIO::~FileIO() {}

FileIO* FileIO::GetInstance() {
  return g_file_io.Pointer();
}

bool FileIO::Write(const std::string& file_name,
                   const shill::ByteString& data) {
  if (base::WriteFile(base::FilePath(file_name),
                      reinterpret_cast<const char*>(data.GetConstData()),
                      data.GetLength()) == -1) {
    return false;
  }
  return true;
}

bool FileIO::Read(const std::string& file_name, shill::ByteString* out_data) {
  std::string buffer;
  if (!base::ReadFileToString(base::FilePath(file_name),
                              &buffer)) {
    return false;
  }
  *out_data = shill::ByteString(buffer, false);
  return true;
}

bool FileIO::PathExists(const std::string& file_name) {
  return base::PathExists(base::FilePath(file_name));
}

bool FileIO::DeleteFile(const std::string& file_name) {
  return base::DeleteFile(base::FilePath(file_name), false);
}

}  // namespace dhcp_client
